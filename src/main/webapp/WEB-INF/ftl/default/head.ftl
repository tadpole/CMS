<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>${SYS_SITENAME}</title>

    <!-- Bootstrap core CSS -->
    <link href="${basePath}/default/css/bootstrap.min.css" rel="stylesheet">
    <link href="${basePath}/default/css/theme.css" rel="stylesheet">
    <link href="${basePath}/default/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="${basePath}/default/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="${basePath}/default/css/flexslider.css"/>
    <link href="${basePath}/default/assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link href="${basePath}/default/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />

    <link rel="stylesheet" href="${basePath}/default/assets/revolution_slider/css/rs-style.css" media="screen">
    <link rel="stylesheet" href="${basePath}/default/assets/revolution_slider/rs-plugin/css/settings.css" media="screen">

    <!-- Custom styles for this template -->
    <link href="${basePath}/default/css/style.css" rel="stylesheet">
    <link href="${basePath}/default/css/style-responsive.css" rel="stylesheet" />
    <script src="${basePath}/default/js/jquery.js"></script>
    <script src="${basePath}/default/js/jquery.form.js"></script>
    <script src="${basePath}/default/js/jquery.validate.js"></script>  
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
     <!--header start-->
    <header class="header-frontend">
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${basePath}/">${SYS_SITENAME}<span style="font-size: 14px; margin-left: 10px;">${SYS_SITEDESC}</span></a>
                </div>            
            </div>
        </div>
    </header>
    <!--header end-->